var mongoDb = require('mongodb').MongoClient;

var uri = "mongodb://bw2:bw2123@cluster0-shard-00-00-89stk.mongodb.net:27017,cluster0-shard-00-01-89stk.mongodb.net:27017,cluster0-shard-00-02-89stk.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true";

function createDbConnection(){
  mongoDb.connect(uri, { useNewUrlParser: true }, function(err, db) {
    if(err){
      console.log(err);
    }
    else {
      console.log('Conectado ao banco');
      return db;
    }
 });
}

module.exports = function(){
  return createDbConnection;
}
