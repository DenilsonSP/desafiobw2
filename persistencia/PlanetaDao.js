var mongoDb = require('mongodb').MongoClient;
var mongo = require('mongodb');

function PlanetaDao (app) {
  this._app = app;
}

PlanetaDao.prototype.salvar = function (planeta, callback) {
  console.log('salvando planeta.');

  var uri = this._app.get('URI_DB');
  mongoDb.connect(uri, { useNewUrlParser: true }, function(err, database) {
    if(err){
      console.log(err);
    }
    else {
      console.log('Conectado ao banco');
      var collection = database.db('b2wStarWarsDb').collection('planetas');
      console.log(collection);
      collection.insert(planeta, callback);
    }
 });
};

PlanetaDao.prototype.listar = function(callback){

  var uri = this._app.get('URI_DB');
  mongoDb.connect(uri, { useNewUrlParser: true }, function(err, database) {
    if(err){
      console.log(err);
    }
    else {
      console.log('Conectado ao banco');
      var collection = database.db('b2wStarWarsDb').collection('planetas').find({}).toArray(function(erro, result){
        callback(result);
      });
    }
 });

}

PlanetaDao.prototype.buscar = function(query, callback){
  var uri = this._app.get('URI_DB');
  mongoDb.connect(uri, { useNewUrlParser: true }, function(err, database) {
    if(err){
      console.log(err);
    }
    else {
      console.log('Conectado ao banco');
      if(query._id){
        query._id = new mongo.ObjectID(query._id);
      }
      var collection = database.db('b2wStarWarsDb').collection('planetas').findOne(query, function(erro, result){
        callback(erro, result);
      });
    }
 });
}

PlanetaDao.prototype.delete = function(query, callback){
  var uri = this._app.get('URI_DB');
  mongoDb.connect(uri, { useNewUrlParser: true }, function(err, database) {
    if(err){
      console.log(err);
    }
    else {
      console.log('Conectado ao banco');
      if(query._id){
        query._id = new mongo.ObjectID(query._id);
      }
      var collection = database.db('b2wStarWarsDb').collection('planetas').deleteOne(query, function(erro, result){
        callback(erro, result);
      });
    }
 });
}


module.exports = function(){
    return PlanetaDao;
};
