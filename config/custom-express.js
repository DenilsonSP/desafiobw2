var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');

module.exports = function(){
  var app = new express();

  //app.set('URI_DB','mongodb://bw2:bw2123@cluster0-shard-00-00-89stk.mongodb.net:27017,cluster0-shard-00-01-89stk.mongodb.net:27017,cluster0-shard-00-02-89stk.mongodb.net:27017/b2wStarWarsDb?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true');
  app.set('URI_DB','mongodb+srv://bw2:bw2123@cluster0-89stk.mongodb.net/b2wStarWarsDb?retryWrites=true');
  //app.use(bodyParser.urlencoded({extended: true}));
  app.use(bodyParser.json());
  app.use(expressValidator());

  consign()
  .include('routes')
  .then('persistencia')
  .then('servicos')
  .into(app);

  return app;
}
