//var restify = require('restify');
var client = require('restify-clients');

function SwapiClient(){
  this._client = client.createJsonClient({
    url:'https://swapi.co',
  });
}

SwapiClient.prototype.GetPlanetByName = function(name, callback){
  this._client.get('/api/planets/?search=' + name + '&format=json', callback);
}

module.exports = function(){
  return SwapiClient;
}
