module.exports = function(app) {

  // lista todos os planetas
  app.get('/planetas', function(req, res){
    var planetaDao = new app.persistencia.PlanetaDao(app);
    var swapiClient = new app.servicos.swapiClient();

    planetaDao.listar(function(planetas) {
      res.status(200).json(planetas);
    });
  });

  //salva um novo planeta
  app.post('/planetas/planeta', function(req, res){

    req.assert("nome", "Nome do planeta é obrigatório").notEmpty();
    req.assert("clima", "Clima do planeta é obrigatório").notEmpty();
    req.assert("terreno", "Terreno do planeta é obrigatório").notEmpty();

    var erros = req.validationErrors();
    if(erros){
      console.log('Erros de validação');
      res.status(400).send(erros);
      return;
    }

    var planeta = req.body;
    console.log(planeta);
    var planetaDao = new app.persistencia.PlanetaDao(app);

    planetaDao.salvar(planeta, function(erro, resultado){
      if(erro){
        console.log(erro);
        res.status(500).json(erro);
      }
      else{
        res.location('/planetas/planeta/' + resultado.insertedIds[0]);
        res.status(201).json(resultado);
      }
    })
  });

  // busca um planeta pelo id (via uri) ou pelo nome (via query a exemplo da search da swapi)
  // esse método também traz o número de aparições do planetas em filmes usando a swapi
  app.get('/planetas/planeta/:id?', function(req, res){
    var query = {};
    var planetaDao = new app.persistencia.PlanetaDao(app);
    var swapiClient = new app.servicos.swapiClient();

    if(req.params.id){
      console.log('buscando por id ' + req.params.id);
      query._id = req.params.id
    }
    else{
      console.log('buscando por nome');
      query.nome = req.query.nome
    }
    
    planetaDao.buscar(query, function(err, result){
      if(err){
        console.log(err);
        res.status(500).json(err);
        return;
      }
      if(!result){
        res.status(404).json('recurso não encontrado');
        return;
      }
      swapiClient.GetPlanetByName(result.nome, function(excecao, request, response, retorno) {
        if(excecao){
          console.log(excecao);
          res.status(500).json(excecao);
          return;
        }
        if(retorno.results[0]){
          result.AparicoesEmFilmes = retorno.results[0].films.length;
        }
        res.status(200).json(result);
      });

    });
  });

  //deleta um recurso da api e retorna 204
  app.delete('/planetas/planeta/:id', function(req, res){
    var query = {};
    var planetaDao = new app.persistencia.PlanetaDao(app);
    query._id = req.params.id
    planetaDao.delete(query, function(err, result){
      if(err){
        console.log(err);
        res.status(500).json(err);
      }
      else{
        res.status(204).json(result);
      }
    });

  });
}
